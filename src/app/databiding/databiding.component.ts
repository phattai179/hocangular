import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-databiding',
  templateUrl: './databiding.component.html',
  styleUrls: ['./databiding.component.scss']
})
export class DatabidingComponent implements OnInit {

  message: string = "Hello Tài"
  isActive: boolean = true
  userName: string = "Tài"
  email: string = "tai@gmail.com"


  constructor() { }

  handleChangeMessage(messageRef: HTMLInputElement){
    // alert("hello")
    console.log(messageRef)
    this.message = messageRef.value
  }

  handleChangeUserName(evt: any) {
    console.log(evt.target)
    this.userName = evt.target.value
  }

  ngOnInit(): void {
  }

}
