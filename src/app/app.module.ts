import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

// Import formModule để có thể sử dụng two-way binding
import {FormsModule} from "@angular/forms" 

// import http client Module
import {HttpClientModule} from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// import { HeaderComponent } from './baitap1/header/header.component';
// import { FooterComponent } from './baitap1/footer/footer.component';
// import { ContentsComponent } from './baitap1/contents/contents.component';
// import { SidebarComponent } from './baitap1/sidebar/sidebar.component';
import { Baitap1Module } from './baitap1/baitap1.module';
import { Baitap2Module } from './baitap2/baitap2.module';
import { DatabidingComponent } from './databiding/databiding.component';
import { Baitap4Component } from './baitap4/baitap4.component';
import { StructuralDirectivesComponent } from './structural-directives/structural-directives.component';
import { AttributeDirectivesComponent } from './attribute-directives/attribute-directives.component';
import { Baitap5Module } from './baitap5/baitap5.module';
import { Baitap6Module } from './baitap6/baitap6.module';
import { Baitap7Module } from './baitap7/baitap7.module';
import { Baitap8Module } from './baitap8/baitap8.module';
import { InteractionModule } from './interaction/interaction.module';
import { MainModule } from './main/main.module';

@NgModule({
  declarations: [
    AppComponent,
    DatabidingComponent,
    Baitap4Component,
    StructuralDirectivesComponent,
    AttributeDirectivesComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,

    Baitap1Module,
    Baitap2Module,
    Baitap5Module,
    Baitap6Module,
    Baitap7Module,
    Baitap8Module,
    InteractionModule,
    MainModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
