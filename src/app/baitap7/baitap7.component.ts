import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-baitap7',
  templateUrl: './baitap7.component.html',
  styleUrls: ['./baitap7.component.scss']
})
export class Baitap7Component implements OnInit {

  constructor() { }

  p : number = 1
  mangSanPham: any[] = [
    { MaSP: 1, TenSP: "Sony XZ", Gia: 1000 },
    { MaSP: 2, TenSP: "Sony XZ2", Gia: 1000 },
    { MaSP: 3, TenSP: "HTC U Ultra", Gia: 1000 },
    { MaSP: 4, TenSP: "HTC U12 Plus", Gia: 1000 },
    { MaSP: 5, TenSP: "Iphone XS MAX", Gia: 1000 },
    { MaSP: 6, TenSP: "Iphone XR", Gia: 1000 },
    { MaSP: 7, TenSP: "Xiaomi Mi Note 3", Gia: 9900 },
    { MaSP: 8, TenSP: "Xiaomi Mi 8", Gia: 1000 },
    { MaSP: 9, TenSP: "Galaxy Note 9", Gia: 1000 },
    { MaSP: 10, TenSP: "Galaxy S9 Plus", Gia: 1000 },
    { MaSP: 11, TenSP: "Nokia X9", Gia: 1000 },
  ]

  addProduct = (maSPRef: string, tenSPRef: string, giaRef: string) => {
    
    let index = this.mangSanPham.findIndex(sp => sp.TenSP === tenSPRef)

    console.log('index', index)

    let sanPhamThem : any = {
      MaSP: maSPRef,
      TenSP: tenSPRef,
      Gia: giaRef
    }

    if(index === -1){
      this.mangSanPham.push(sanPhamThem)
      alert("Thêm thành công")
    }else{
      alert("Sản phẩm đã tồn tại")
    }

  }

  ngOnInit(): void {
  }

}
