import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { ContentsComponent } from './contents/contents.component';
import { FooterComponent } from './footer/footer.component';
import { SidebarComponent } from './sidebar/sidebar.component';



@NgModule({
  declarations: [HeaderComponent, ContentsComponent, FooterComponent, SidebarComponent],
  imports: [
    CommonModule
  ],

  exports:[
    HeaderComponent,
    ContentsComponent,
    SidebarComponent,
    FooterComponent
  ]
})
export class Baitap1Module { }
