import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexContentComponentComponent } from './index-content-component.component';

describe('IndexContentComponentComponent', () => {
  let component: IndexContentComponentComponent;
  let fixture: ComponentFixture<IndexContentComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexContentComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexContentComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
