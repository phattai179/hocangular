import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SliderComponentsComponent } from './slider-components.component';

describe('SliderComponentsComponent', () => {
  let component: SliderComponentsComponent;
  let fixture: ComponentFixture<SliderComponentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SliderComponentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SliderComponentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
