import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Baitap2Component } from './baitap2.component';
import { HeaderComponent } from './header/header.component';
import { IndexComponentComponent } from './index-component/index-component.component';
import { SliderComponentsComponent } from './slider-components/slider-components.component';
import { IndexContentComponentComponent } from './index-content-component/index-content-component.component';
import { ItemComponentComponent } from './item-component/item-component.component';
import { FooterComponentComponent } from './footer-component/footer-component.component';



@NgModule({
  declarations: [
    Baitap2Component,
    HeaderComponent,
    IndexComponentComponent,
    SliderComponentsComponent,
    IndexContentComponentComponent,
    ItemComponentComponent,
    FooterComponentComponent
  ],
  imports: [
    CommonModule
  ],

  exports: [
    Baitap2Component
  ]
})
export class Baitap2Module { }
