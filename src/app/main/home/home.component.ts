import { Component, OnInit } from '@angular/core';
import { Movie } from 'src/app/core/models/movie';
import { MovieService } from 'src/app/core/services/movie.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  movieList: Movie[] = []

  constructor(private movieService: MovieService) { }

  ngOnInit(): void {

    this.movieService.getMovieList().subscribe({
      next: (result) => {
        console.log('result', result)
        this.movieList = result
      },

      error: (error) => {
        console.log(error)
      },

      complete: () => {
        console.log('Fectching complete')
      }
    })


  }

}
