import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { LayoutComponent } from './layout/layout.component';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { MainRoutingModule } from './main-routing.module';
import { PipeModule } from '../pipe/pipe.module';
import { DirectiveModule } from '../directive/directive.module';



@NgModule({
  declarations: [
    HomeComponent,
    LayoutComponent,
    SigninComponent,
    SignupComponent
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    PipeModule,
    DirectiveModule,
  ],
  exports: [
    HomeComponent,
    LayoutComponent,
    SigninComponent,
    SignupComponent
  ]
})
export class MainModule { }
