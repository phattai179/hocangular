import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "./home/home.component";
import { LayoutComponent } from "./layout/layout.component";
import { SigninComponent } from "./signin/signin.component";
import { SignupComponent } from "./signup/signup.component";



const routes: Routes = [

    {path: "", component: LayoutComponent, 
      children: [
          {path: "signin", component: SigninComponent},
          {path: "signup", component: SignupComponent},
          {path: "", component: HomeComponent}
      ]

    }

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MainRoutingModule { }




