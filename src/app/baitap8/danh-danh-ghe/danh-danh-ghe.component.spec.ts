import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DanhDanhGheComponent } from './danh-danh-ghe.component';

describe('DanhDanhGheComponent', () => {
  let component: DanhDanhGheComponent;
  let fixture: ComponentFixture<DanhDanhGheComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DanhDanhGheComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DanhDanhGheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
