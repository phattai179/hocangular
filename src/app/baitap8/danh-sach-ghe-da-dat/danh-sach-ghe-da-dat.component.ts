import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-danh-sach-ghe-da-dat',
  templateUrl: './danh-sach-ghe-da-dat.component.html',
  styleUrls: ['./danh-sach-ghe-da-dat.component.scss']
})
export class DanhSachGheDaDatComponent implements OnInit {

  @Output () onRemove = new EventEmitter()

  danhSachGheDaDat: any[] = []

  constructor() { }

  ngOnInit(): void {
  }

  chonGhe = (gheChon: any) => {
    if(gheChon.dangChon){
      this.danhSachGheDaDat.push(gheChon)
    }else{
      this.danhSachGheDaDat = this.danhSachGheDaDat.filter(ghe => ghe.SoGhe !== gheChon.SoGhe)
    }
  }

  huyGhe = (soGhe : any) => {
      this.danhSachGheDaDat = this.danhSachGheDaDat.filter(ghe => ghe.SoGhe !== soGhe)
      
      this.onRemove.emit(soGhe)
  }



}
