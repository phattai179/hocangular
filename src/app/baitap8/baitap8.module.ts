import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Baitap8Component } from './baitap8.component';
import { DanhDanhGheComponent } from './danh-danh-ghe/danh-danh-ghe.component';
import { DanhSachGheDaDatComponent } from './danh-sach-ghe-da-dat/danh-sach-ghe-da-dat.component';
import { GheComponent } from './ghe/ghe.component';



@NgModule({
  declarations: [
    Baitap8Component,
    DanhDanhGheComponent,
    DanhSachGheDaDatComponent,
    GheComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    Baitap8Component,
    DanhDanhGheComponent,
    DanhSachGheDaDatComponent,
    GheComponent
  ]
})
export class Baitap8Module { }
