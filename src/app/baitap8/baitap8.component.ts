import { Component, OnInit, ViewChild } from '@angular/core';
import { DanhDanhGheComponent } from './danh-danh-ghe/danh-danh-ghe.component';
import { DanhSachGheDaDatComponent } from './danh-sach-ghe-da-dat/danh-sach-ghe-da-dat.component';

@Component({
  selector: 'app-baitap8',
  templateUrl: './baitap8.component.html',
  styleUrls: ['./baitap8.component.scss']
})
export class Baitap8Component implements OnInit {

  @ViewChild ('dsGheDangDat') dsGheDaDatComponent !: DanhSachGheDaDatComponent

  @ViewChild ('dsGhe') dsGheComponent !: DanhDanhGheComponent
  constructor() { }

  ngOnInit(): void {
  }

  chonGhe = (ghe: any) => {
    this.dsGheDaDatComponent.chonGhe(ghe)
  }

  huyGhe = (soGheHuy: any) => {
    this.dsGheComponent.huyGhe(soGheHuy)
  }

}
