import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-movie-item',
  templateUrl: './movie-item.component.html',
  styleUrls: ['./movie-item.component.scss']
})
export class MovieItemComponent implements OnInit {

  @Input () movie: any
  @Output () onDelete = new EventEmitter() 

  constructor() { }

  ngOnInit(): void {
  }

  deleteMovie = (movieId: any) => {
    this.onDelete.emit(movieId)
  }



}
