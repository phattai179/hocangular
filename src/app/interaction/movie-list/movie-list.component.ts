import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss']
})
export class MovieListComponent implements OnInit {

  constructor() { }

  movieList : any[] = [
    { id: 1, name: 'Aventure', price: 1000, image: "./assets/img/1.jpg" },
    { id: 2, name: 'Fast and Serious', price: 2000, image: "./assets/img/2.jpg" },
    { id: 3, name: 'Tom and Jerry', price: 3000, image: "./assets/img/3.jpg" },
    { id: 4, name: 'The Magic', price: 4000, image: "./assets/img/4.jpg" },
  ]

  ngOnInit(): void {
  }

  deleteMovie = (movieId : any) => {
    this.movieList = this.movieList.filter(movie => movie.id !== movieId)
  }

  

}
