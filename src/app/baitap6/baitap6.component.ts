import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-baitap6',
  templateUrl: './baitap6.component.html',
  styleUrls: ['./baitap6.component.scss']
})
export class Baitap6Component implements OnInit {

  constructor() { }

  // any[]
  mangSanPham: Array<any> = []

  themSanPham = (maSPRef: string, tenSPRef: string, giaSPRef: string) => {

    let sanPham : any = {
      maSP: maSPRef,
      tenSP: tenSPRef,
      giaSP: giaSPRef
    }

    this.mangSanPham.push(sanPham)
    console.log('mangSanPham', this.mangSanPham)
  }

  ngOnInit(): void {
  }

}
