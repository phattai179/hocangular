import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http'
import {Observable} from 'rxjs'
import { Movie } from '../models/movie';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  constructor(private http: HttpClient, private api: ApiService) { }

  getMovieList = () : Observable<Movie[]> => {
    
    // const url = `https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim`

    // let params = new HttpParams()
    // params = params.append('maNhom', 'GP01')

    // return this.http.get<Movie[]>(url,{params})

    const url = `QuanLyPhim/LayDanhSachPhim`;
    let params = new HttpParams()
    params = params.append('maNhom', 'GP01');

    return this.api.get<Movie[]>(url, {params}) 

  }
}
