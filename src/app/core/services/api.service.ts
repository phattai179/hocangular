import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {


  baseUrl: string = `https://movie0706.cybersoft.edu.vn/api/`;
  constructor( private http: HttpClient) { }

  get<T>(path: string, options = {}):Observable<T> {
    console.log(options)
    
    return this.http.get<T>(`${this.baseUrl}/${path}`, options)
  }


}
