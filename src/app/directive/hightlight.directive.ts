import { Directive, ElementRef, HostListener, OnInit } from '@angular/core';

@Directive({
  selector: '[appHightlight]'
})
export class HightlightDirective {

  constructor(private elementRef: ElementRef) {
    // Thêm style 
    this.elementRef.nativeElement.style.backgroundColor = 'red';

    // Thêm class
    this.elementRef.nativeElement.classList.add('w-25')
    
    // Kiểm tra chiều dài và chiều rộng của thẻ

    console.log(this.elementRef.nativeElement.clientWidth)
    console.log(this.elementRef.nativeElement.clientHeight)

  }

  @HostListener ('mouseenter') handleMouseEnter() {
    this.elementRef.nativeElement.style.backgroundColor = 'green'
  }

  @HostListener ('mouseleave') handleMouseLeave() {
    this.elementRef.nativeElement.style.backgroundColor = 'red'
  }

  @HostListener ('click') handleClick () {
    this.elementRef.nativeElement.classList.remove('w-25')
    this.elementRef.nativeElement.classList.add('w-100')
  }

  // ngOnInit(){
  //   console.log(this.elementRef.nativeElement.clientWidth)
  //   console.log(this.elementRef.nativeElement.clientHeight)
  // }

}
